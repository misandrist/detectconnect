# Overview

This has two parts. The first just periodically checks a known-good
url to see that it returns a 200 response and the right content, and
if it gets a redirect it'll open that in a browser.

The other part is listening on D-Bus for network changes, and then
immediately checking.
